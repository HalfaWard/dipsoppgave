﻿using System;
using System.IO;
using System.Linq;
using System.Text;

namespace DipsOppgave
{
    public class TodoList
    {
        private const string CompletePath = "TodoList.txt";

        public static void Main(string[] args)
        {
            var app = new TodoList();
            app.CheckForFile();
            Console.WriteLine("To-do app (Write '?' for options)");
            while (true)
            {
                var inputTokens = Console.ReadLine().Split(": ");
                app.ReadInput(inputTokens);
            }
        }

        public bool ReadInput(string[] actionToken)
        {
            switch (actionToken[0].ToLower())
            {
                case "?":
                    Console.Write("Options: \n" +
                                  "Add: \"Input\" (Adds To-do in list) \n" +
                                  "Do: \"Number\" (Sets To-do at index done and removes it)\n" +
                                  "Print (Prints remaining list of To-do's)\n");
                    return true;
                case "add":
                    if (actionToken.Length > 1)
                    {
                        AddTask(actionToken[1]);
                        return true;
                    }
                    else
                    {
                        Console.WriteLine("Invalid input, needs Todo text");
                        return false;
                    }
                case "do":
                    if (actionToken.Length > 1 && actionToken[1].Any(char.IsDigit))
                    {
                        DoTask(int.Parse(actionToken[1]));
                        return true;
                    }
                    else
                    {
                        Console.WriteLine("Invalid input, needs task number");
                        return false;
                    }
                case "print":
                    PrintTodoList();
                    return true;
                default:
                    Console.WriteLine("Invalid input, try again");
                    return false;
            }
        }

        private void PrintToConsole(string print)
        {
            Console.Write(print);
        }

        public string PrintTodoList()
        {
            string[] file = ReadFile().Split(";");
            string printString = "";
            if (file.Length > 1)
            {
                for (int i = 1; i < file.Length; i++)
                {
                    if(file[i-1].Length != 0)printString += i + ". \"" + file[i - 1] + "\"\n";
                }
            }
            else
            {
                printString = "Your Todo-list is empty\n";   
            }
            PrintToConsole(printString); 
            return printString;
        }

        public void AddTask(string todo)
        {
            var file = ReadFile();
            File.AppendAllText(CompletePath, todo + ";", Encoding.UTF8);
            Console.WriteLine("Added: \"" + todo + "\" to list");
        }

        public void DoTask(int taskNumber)
        {
            string[] file = ReadFile().Split(";");
            string removedString = file[taskNumber - 1];
            file = file.Where((source, index) => index != taskNumber - 1).ToArray();
            string newList = "";
            foreach (string todo in file)
            {
                if(todo.Length!=0)newList += todo + ";";
            }
            File.WriteAllText(CompletePath, newList);
            Console.WriteLine("Set TodoItem number " + taskNumber + " \"" + removedString +
                              "\" to done and removed from list");
        }

        private string ReadFile()
        {
            return File.ReadAllText(CompletePath);
        }

        public bool CheckForFile()
        {
            if (File.Exists(CompletePath)) return true;
            using (Stream str = File.Create(CompletePath))
            {
                str.Close();
            }
            Console.WriteLine("List did not exist but has been created");
            return false;
        }
    }
}