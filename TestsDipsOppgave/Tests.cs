using System;
using System.IO;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using DipsOppgave;
using Microsoft.VisualStudio.TestPlatform.ObjectModel;

namespace TestsDipsOppgave
{
    [TestClass]
    public class Tests
    {
        private TodoList todoList;
        private string completePath = "TodoList.txt";
        
        [TestInitialize]
        public void SetUp()
        {
            todoList = new TodoList();
            todoList.CheckForFile();
            File.WriteAllText(completePath, "");
        }

        [TestMethod]
        public void ReadInput_ValidInput_true()
        {
            string[] inputOptions = {"?"};
            Assert.IsTrue(todoList.ReadInput(inputOptions));
            string[] inputAdd = {"Add", "Todo"};
            Assert.IsTrue(todoList.ReadInput(inputAdd));
            string[] inputDo = {"Do", "1"};
            Assert.IsTrue(todoList.ReadInput(inputDo));
            string[] inputPrint = {"Print"};
            Assert.IsTrue(todoList.ReadInput(inputPrint));

        }
        
        [TestMethod]
        public void ReadInput_InvalidInput_false()
        {
            string[] inputDefault = {"invalid input"};
            Assert.IsFalse(todoList.ReadInput(inputDefault));
            string[] inputAdd = {"Add"};
            Assert.IsFalse(todoList.ReadInput(inputAdd));
            string[] inputDo = {"Do", "abc"};
            Assert.IsFalse(todoList.ReadInput(inputDo));
        }

        [TestMethod]
        public void AddTask_taskAdded_printReturnsTask()
        {
            todoList.AddTask("Task one");
            Assert.AreEqual("1. \"Task one\"\n", todoList.PrintTodoList());
        }
        
        [TestMethod]
        public void AddTask_threeTasksAdded_printReturnsTasks()
        {
            todoList.AddTask("Task one");
            todoList.AddTask("Task two");
            todoList.AddTask("Task three");
            Assert.AreEqual("1. \"Task one\"\n2. \"Task two\"\n3. \"Task three\"\n", todoList.PrintTodoList());
        }
        
        [TestMethod]
        public void DoTask_addTasksMiddleRemoved_printReturnsTasks()
        {
            todoList.AddTask("Task one");
            todoList.AddTask("Task two");
            todoList.AddTask("Task three");
            todoList.DoTask(2);
            Assert.AreEqual("1. \"Task one\"\n2. \"Task three\"\n", todoList.PrintTodoList());
        }
        
        [TestMethod]
        public void DoTask_addTasksFirstRemoved_printReturnsTasks()
        {
            todoList.AddTask("Task one");
            todoList.AddTask("Task two");
            todoList.AddTask("Task three");
            todoList.DoTask(1);
            Assert.AreEqual("1. \"Task two\"\n2. \"Task three\"\n", todoList.PrintTodoList());
        }
        
        [TestMethod]
        public void PrintTodoList_emptyTodolist_expectedString()
        {
            Assert.AreEqual("Your Todo-list is empty\n", todoList.PrintTodoList());
        }
        
        [TestMethod]
        public void checkForFile_fileExists_true()
        {
            todoList.CheckForFile();
            Assert.IsTrue(File.Exists(completePath));
        }
        
        [TestMethod]
        public void CheckForFile_deleteFile_createdNewOne()
        {
            File.Delete	(completePath);
            Assert.IsFalse(todoList.CheckForFile());
            Assert.IsTrue(File.Exists(completePath));
        }
    }
}